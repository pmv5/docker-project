<?php 

// Obtain host, username and password from env
$host     = 'mysql';
$user     = 'root';
$password = getenv("MYSQL_ROOT_PASSWORD");
$db       = 'employeedatabase';

// Create connection to mysql database
$conn = new mysqli($host, $user, $password,$db);

// Testing connection
echo "Testing mysql connection...";
echo "</br>";  
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected to MySQL successfully!";
echo "</br>";  

// Select employee id and name of employees
$sql = "SELECT empid, name FROM employeestable";
$result = $conn->query($sql);

// Display employee id and name of each employee
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    echo "Employee id: " . $row["empid"]. " - Name: " . $row["name"]. "<br>";
  }
} else {
  echo "There are no entries";
}

// Close connection
$conn->close();
?>
