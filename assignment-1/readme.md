# STEPS

### 1. Build a custom apache image
 
    docker build . -t apache2 


### 2. Create a custom network
 
     docker network create 
     --driver bridge 
     --subnet 172.18.0.1/24 
     --gateway 172.18.0.1/24 mynetwork 


### 3. Run mysql container
 
    docker run 
    -p 3306:3306 
    -e MYSQL_ROOT_PASSWORD=rootpassword 
    -d 
    --network mynetwork 
    --name mysql  
    --default-authentication-plugin=mysql_native_password 
    mysql


### 4. Enter into mysql container

    docker exec -it mysql bash 

### 5. Create database, table and insert values 
 
    mysql -u root -p
    
    CREATE DATABASE employeedatabase;

    USE employeedatabase;

    CREATE TABLE employeestable(empid int, name VARCHAR(50), designation VARCHAR(50));

    INSERT INTO employeestable VALUES (1, "priya", "associate cloud engineer");

    INSERT INTO employeestable VALUES (2, "vaish", "associate engineer");


### 6. Run apache2
 
    docker run 
    -p 8080:80 
    --name apache2container 
    -e MYSQL_ROOT_PASSWORD=rootpassword 
    -d 
    --network mynetwork 
    apache2 


