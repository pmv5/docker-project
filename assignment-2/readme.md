# STEPS

### 1. Build a custom apache image
 
    docker build . -t apache2 


### 2. Create a custom network
 
     docker network create 
     --driver bridge 
     --subnet 172.18.0.1/24 
     --gateway 172.18.0.1/24 mynetwork 


### 3. Create a file to store password
 
    Create a password.txt file to store MYSQL_ROOT_PASSWORD


### 4. Execute docker-compose.yml

    docker compose up -d

### 5. Enter into mysql container

    docker exec -it mysql1 bash 

### 6. Create database, table and insert values 
 
    mysql -u root -p
    
    CREATE DATABASE employeedatabase;

    USE employeedatabase;

    CREATE TABLE employeestable(empid int, name VARCHAR(50), designation VARCHAR(50));

    INSERT INTO employeestable VALUES (1, "priya", "associate cloud engineer");

    INSERT INTO employeestable VALUES (2, "vaish", "associate engineer");